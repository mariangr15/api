<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::get('index', [ProductController::class, 'index']);
Route::post('store', [ProductController::class, 'store']);
Route::post('update', [ProductController::class, 'update']);
Route::post('delete', [ProductController::class, 'destroy']);

